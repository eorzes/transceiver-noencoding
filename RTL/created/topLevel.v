`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.01.2023 16:52:37
// Design Name: 
// Module Name: topLevel
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module topLevel(
    input refClkP,
    input refClkN,
    
    input sysClkP,
    input sysClkN,
    
    input GTXn_in,
    input GTXp_in,
    output GTXn_out,
    output GTXp_out,
    
    output SFP0_disable
  );
    
  wire clk100;
  wire locked;
  assign reset = !locked;
  
  //wire rxDataIsAll_1; // il PRBS se riceve tutti 0 genera tutti 0 come parola successiva.
  //wire status; // 1 se il PRBS checker riceve gli stessi dati che ha inviatpo il PRBS che trasmette, ricevuto il primo ricava il successivo e controlla che coincida.
  //assign transferIsAllCorrect = !rxDataIsAll_1 & status;
    
  clk_wiz_0 clockMMCM (
     // Clock out ports
     .clk_out1(clk100),     // output clk_out1
     // Status and control signals
     .locked(locked),       // output locked
     // Clock in ports
     .clk_in1_p(sysClkP),    // input clk_in1_p
     .clk_in1_n(sysClkN)     // input clk_in1_n
  );   
    
  gtwizard_ultrascale_0_example_top txModel (
     // Differential reference clock inputs
     .mgtrefclk0_x0y2_p(refClkP),
     .mgtrefclk0_x0y2_n(refClkN),

     // Serial data ports for transceiver channel 0
     .ch0_gthrxn_in(GTXn_in),
     .ch0_gthrxp_in(GTXp_in),
     .ch0_gthtxn_out(GTXn_out),
     .ch0_gthtxp_out(GTXp_out),

     // User-provided ports for reset helper block(s)
     .hb_gtwiz_reset_clk_freerun_in(clk100),
     .hb_gtwiz_reset_all_in(reset),

     // PRBS-based link status ports
     .link_down_latched_reset_in(0),
     .link_status_out(),
     .link_down_latched_out()
  );

assign SFP0_disable = 1'b0;  // occorre connettere la fibra alla porta SFP


endmodule