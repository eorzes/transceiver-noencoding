
## Getting started

In order to set up the Vivado environment you can:

- [ ] Launch Vivado in tcl mode inside the repository root directory: `vivado -mode tcl`
- [ ] Generate the project by sourcing the TCL file: `source generate_project.tcl`
- [ ] Start the GUI: `start_gui`

Remember to regenerate the TCL script when you add or remove new sources / IPs / constraints etc, by using the command `write_project_tcl -force ../generate_project.tcl`.
